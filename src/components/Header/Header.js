import React, { Component } from 'react';
import userIcon from '../../assets/user.svg'
import './Header.css';
import '../BodyPart/BodyPart.css';
import $ from 'jquery';




class Header extends Component {
    componentDidMount(){
        $(".navbar-brand").click(function(){
            $(".left-nav").toggleClass( "hideShow-nav" );
            $('.Bodypart-wrapper').toggleClass("ml-lg-386");
            $('.Bodypart-wrapper').toggleClass("ml-40r");
        });
        $(".xs-button").click(function(){
            $(".left-nav").toggleClass( "hideShow-nav-small" );
            $('.Bodypart-wrapper').toggleClass("ml-386");
        });
        $('.dropdown-menu').on('click', function(e) {
            if($(this).hasClass('dropdown-menu-form')) {
                e.stopPropagation();
            }
        });
        $("#accordian a").click(function() {
            var link = $(this);
            var closest_ul = link.closest("ul");
            var parallel_active_links = closest_ul.find(".active")
            var closest_li = link.closest("li");
            var link_status = closest_li.hasClass("active");
            var count = 0;

            // $(".fa-angle-right").css("transform","rotate(90deg)")
            closest_ul.find("ul").slideUp(function() {
                    if (++count == closest_ul.find("ul").length)
                            parallel_active_links.removeClass("active");
                            
            });
            

            if (!link_status) {
                    closest_li.children("ul").slideDown();
                    closest_li.addClass("active");
            }
        })
    }
  render() {
    return (
      <div>
        <nav className="navbar navbar-default">
            <div className="container-fluid">
                <div className="navbar-header">
                    <a className="navbar-brand hidden-xs" href="#"><i className="fas fa-bars"></i></a>
                    <a className="navbar-brand xs-button hidden-md hidden-sm hidden-lg" href="#"><i className="fas fa-bars"></i></a>
                </div>
                <ul className="nav navbar-nav pull-left">
                    <li>
                        <a href="#" className="search-wrapper">
                            <input type="text" className="search-box"/>
                            <i className="fas fa-search"></i>
                        </a>
                    </li>
                </ul>
                <ul className="nav navbar-nav pull-right">
                    <li>
                        <a href="#"><i className="fas fa-user-plus"></i>INVITE TEAM MEMBER</a>
                    </li>
                    <li><a href="#"><i className="fas fa-bell"></i></a></li>
                    <li className="profile-image-wrapper dropdown">
                        <a href="#" className="dropdown-toggle" id="menu1" data-toggle="dropdown">
                            <img src={userIcon} className="profile-image"/>
                        </a>
                        <ul className="dropdown-menu dropdown-menu-form" role="menu" aria-labelledby="menu1">
                            <li role="presentation">
                                <a role="menuitem" className="pull-left" href="#">Dark Mode</a>
                                <div className="pull-right switch-btn">
                                    <label className="switch">
                                        <input type="checkbox" checked />
                                        <span className="slider round"></span>
                                    </label>
                                </div>    
                            </li>
                            <li role="presentation"><a role="menuitem" href="#">Profile</a></li>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a role="menuitem" href="#">What's new</a></li>
                            <li role="presentation"><a role="menuitem" href="#">Help</a></li>
                            <li role="presentation"><a role="menuitem" href="#">Send Feedback</a></li>
                            <li role="presentation"><a role="menuitem" href="#">Hints and shortcuts</a></li>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a role="menuitem" href="#">Log out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <div className="box-shadow"></div>
        <div className="left-nav">
            <ul className="nav nav-tabs side-nav-tabs" role="tablist">
                <li className="active">
                    <a href="#home" role="tab" data-toggle="tab">All</a>
                </li>
                <li><a href="#profile" role="tab" data-toggle="tab">Board</a>
                </li>
                <li>
                    <a href="#messages" role="tab" data-toggle="tab">Graph</a>
                </li>
                <li>
                    <a href="#settings" role="tab" data-toggle="tab">Recent</a>
                </li>
                <li>
                    <a href="#"><i className="fas fa-ellipsis-v"></i></a>
                </li>
            </ul>
            <div className="tab-content">
                <div className="tab-pane fade active in" id="home">
                    <div className="row top-toolbar">
                        <div className="col-sm-12">
                            <div className="pull-left ptb-15">
                                <span>All</span>
                            </div>
                            <div className="pull-right">
                                <ul className="title-action-icons">
                                    <li className="active">
                                        <a href="#" ><i className="fas fa-plus"></i></a>
                                    </li>
                                    <li><a href="#"><i className="fas fa-expand"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i className="fas fa-angle-double-left"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="row plr-30">
                        <div className="col-sm-12">
                            {/* <ul className="sideNav-menu-items-list-wrapper">
                                <li><i className="fas fa-angle-right"></i>Collection.1</li>
                                <li><i className="fas fa-angle-right"></i>Collection.2</li>
                                <li><i className="fas fa-angle-right"></i>Collection.3</li>
                                <li><i className="fas fa-angle-right"></i>Collection.4</li>
                                <li><i className="fas fa-angle-right"></i>Collection.5</li>
                            </ul> */}
                            <div id="accordian">
                                <ul className="sideNav-menu-items-list-wrapper">
                                    <li>
                                        <a href="#"><i className="fas fa-angle-right"></i>Collection.1</a>
                                        <span className="pull-right acordion-icon-btn-wrapper">
                                            <button type="button"><i class="far fa-clone"></i></button>
                                        </span>
                                        <ul>
                                            <li>
                                                <a href="#"><i className="fas fa-angle-right"></i>Collection.1.1</a>
                                                <span className="pull-right acordion-icon-btn-wrapper">
                                                    <button type="button"><i class="far fa-clone"></i></button>
                                                </span>
                                                <ul>
                                                    <li>
                                                        <a href="#"><i className="fas fa-angle-right"></i>Collection.1.1.1</a>
                                                        <span className="pull-right acordion-icon-btn-wrapper">
                                                            <button type="button"><i className="fas fa-plus"></i></button>
                                                            <button type="button"><i className="fas fa-clone"></i></button>
                                                            <button type="button"><i className="fas fa-ellipsis-v"></i></button>
                                                        </span>
                                                        <ul>
                                                            <li><a href="#">Content Page 1.1.1.1</a></li>
                                                            <li><a href="#">Quiz</a></li>
                                                            <li><a href="#">Videos</a></li>
                                                            <li><a href="#">WYSIWYG Editor</a></li>
                                                            <li><a href="#">Settings</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#"><i className="fas fa-angle-right"></i>Collection.1.2</a>
                                                <span className="pull-right acordion-icon-btn-wrapper">
                                                    <button type="button"><i class="far fa-clone"></i></button>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><i className="fas fa-angle-right"></i>Collection.2</a>
                                        <span className="pull-right acordion-icon-btn-wrapper">
                                            <button type="button"><i class="far fa-clone"></i></button>
                                        </span>
                                        <ul>
                                            <li>
                                                <a href="#"><i className="fas fa-angle-right"></i>Collection.2.1</a>
                                                <span className="pull-right acordion-icon-btn-wrapper">
                                                    <button type="button"><i class="far fa-clone"></i></button>
                                                </span>
                                                <ul>
                                                    <li>
                                                        <a href="#"><i className="fas fa-angle-right"></i>Collection.2.1.1</a>
                                                        <span className="pull-right acordion-icon-btn-wrapper">
                                                            <button type="button"><i className="fas fa-plus"></i></button>
                                                            <button type="button"><i className="fas fa-clone"></i></button>
                                                            <button type="button"><i className="fas fa-ellipsis-v"></i></button>
                                                        </span>
                                                        <ul>
                                                            <li><a href="#">Content Page 2.1.1.1</a></li>
                                                            <li><a href="#">Quiz</a></li>
                                                            <li><a href="#">Videos</a></li>
                                                            <li><a href="#">WYSIWYG Editor</a></li>
                                                            <li><a href="#">Settings</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#"><i className="fas fa-angle-right"></i>Collection.2.2</a>
                                                <span className="pull-right acordion-icon-btn-wrapper">
                                                    <button type="button"><i class="far fa-clone"></i></button>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><i className="fas fa-angle-right"></i>Collection.3</a>
                                        <span className="pull-right acordion-icon-btn-wrapper">
                                            <button type="button"><i class="far fa-clone"></i></button>
                                        </span>
                                        <ul>
                                            <li>
                                                <a href="#"><i className="fas fa-angle-right"></i>Collection.3.1</a>
                                                <span className="pull-right acordion-icon-btn-wrapper">
                                                    <button type="button"><i class="far fa-clone"></i></button>
                                                </span>
                                                <ul>
                                                    <li>
                                                        <a href="#"><i className="fas fa-angle-right"></i>Collection.3.1.1</a>
                                                        <span className="pull-right acordion-icon-btn-wrapper">
                                                            <button type="button"><i className="fas fa-plus"></i></button>
                                                            <button type="button"><i className="fas fa-clone"></i></button>
                                                            <button type="button"><i className="fas fa-ellipsis-v"></i></button>
                                                        </span>
                                                        <ul>
                                                            <li><a href="#">Content Page 3.1.1.1</a></li>
                                                            <li><a href="#">Quiz</a></li>
                                                            <li><a href="#">Videos</a></li>
                                                            <li><a href="#">WYSIWYG Editor</a></li>
                                                            <li><a href="#">Settings</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><i className="fas fa-angle-right"></i>Collection.4</a>
                                        <span className="pull-right acordion-icon-btn-wrapper">
                                            <button type="button"><i class="far fa-clone"></i></button>
                                        </span>
                                        <ul>
                                            <li>
                                                <a href="#"><i className="fas fa-angle-right"></i>Collection.4.1</a>
                                                <span className="pull-right acordion-icon-btn-wrapper">
                                                    <button type="button"><i class="far fa-clone"></i></button>
                                                </span>
                                                <ul>
                                                    <li>
                                                        <a href="#"><i className="fas fa-angle-right"></i>Collection.4.1.1</a>
                                                        <span className="pull-right acordion-icon-btn-wrapper">
                                                            <button type="button"><i className="fas fa-plus"></i></button>
                                                            <button type="button"><i className="fas fa-clone"></i></button>
                                                            <button type="button"><i className="fas fa-ellipsis-v"></i></button>
                                                        </span>
                                                        <ul>
                                                            <li><a href="#">Content Page 4.1.1.1</a></li>
                                                            <li><a href="#">Quiz</a></li>
                                                            <li><a href="#">Videos</a></li>
                                                            <li><a href="#">WYSIWYG Editor</a></li>
                                                            <li><a href="#">Settings</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tab-pane fade" id="profile">
                    <div className="row top-toolbar">
                        <div className="col-sm-12">
                            <div className="pull-left ptb-15">
                                <span>Board</span>
                            </div>
                            <div className="pull-right">
                                <ul className="title-action-icons">
                                    <li className="active">
                                        <a href="#" ><i className="fas fa-plus"></i></a>
                                    </li>
                                    <li><a href="#"><i className="fas fa-expand"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i className="fas fa-angle-double-left"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="row plr-30">
                        <div className="col-sm-12">
                            <ul className="sideNav-menu-items-list-wrapper">
                                <li><i className="fas fa-angle-right"></i>Collection.1</li>
                                <li><i className="fas fa-angle-right"></i>Collection.2</li>
                                <li><i className="fas fa-angle-right"></i>Collection.3</li>
                                <li><i className="fas fa-angle-right"></i>Collection.4</li>
                                <li><i className="fas fa-angle-right"></i>Collection.5</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="tab-pane fade" id="messages">
                    <div className="row top-toolbar">
                        <div className="col-sm-12">
                            <div className="pull-left ptb-15">
                                <span>Graph</span>
                            </div>
                            <div className="pull-right">
                                <ul className="title-action-icons">
                                    <li className="active">
                                        <a href="#" ><i className="fas fa-plus"></i></a>
                                    </li>
                                    <li><a href="#"><i className="fas fa-expand"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i className="fas fa-angle-double-left"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="row plr-30">
                        <div className="col-sm-12">
                            <ul className="sideNav-menu-items-list-wrapper">
                                <li><i className="fas fa-angle-right"></i>Collection.1</li>
                                <li><i className="fas fa-angle-right"></i>Collection.2</li>
                                <li><i className="fas fa-angle-right"></i>Collection.3</li>
                                <li><i className="fas fa-angle-right"></i>Collection.4</li>
                                <li><i className="fas fa-angle-right"></i>Collection.5</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="tab-pane fade" id="settings">
                    <div className="row top-toolbar">
                        <div className="col-sm-12">
                            <div className="pull-left ptb-15">
                                <span>Recent</span>
                            </div>
                            <div className="pull-right">
                                <ul className="title-action-icons">
                                    <li className="active">
                                        <a href="#" ><i className="fas fa-plus"></i></a>
                                    </li>
                                    <li><a href="#"><i className="fas fa-expand"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i className="fas fa-angle-double-left"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="row plr-30">
                        <div className="col-sm-12">
                            <ul className="sideNav-menu-items-list-wrapper">
                                <li><i className="fas fa-angle-right"></i>Collection.1</li>
                                <li><i className="fas fa-angle-right"></i>Collection.2</li>
                                <li><i className="fas fa-angle-right"></i>Collection.3</li>
                                <li><i className="fas fa-angle-right"></i>Collection.4</li>
                                <li><i className="fas fa-angle-right"></i>Collection.5</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      </div>
    );
  }
}

export default Header;