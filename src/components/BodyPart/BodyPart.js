import React, { Component } from 'react';
import './BodyPart.css';
// import $ from 'jquery';
class BodyPart extends Component {
    
  render() {

    

    return (
      <div className="Bodypart-wrapper ml-40r">
        <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <span className="menu-map">collection.1/collection.1.1/collection.1.1.1</span>
                    <a href="#" className="add-item link-btn">Add item member</a>
                </div>
            </div>
            <div className="clearfix"></div>
            <div className="row">
                <div className="col-sm-12 editor">
                    <div className="topic-title">
                        <h4>WYSIWYG Editor</h4>
                    </div>
                    <textarea id="eg-dark-theme" className="froala-editor">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                </div>
                <div className="clearfix"></div>
                <div className="col-sm-12 editor">
                    <div className="topic-title">
                        <h4>Topic Name</h4>
                    </div>
                    <textarea id="eg-dark-theme" className="froala-editor2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</textarea>
                </div>
            </div>
        </div>
      </div>
    );
  }
}

export default BodyPart;
