import React, { Component } from 'react';
import Header from '../components/Header/Header';
import BodyPart from '../components/BodyPart/BodyPart';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
      <Header />
      <BodyPart />
      </div>
    );
  }
}

export default App;
